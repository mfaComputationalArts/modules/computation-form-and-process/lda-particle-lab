#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  _setupGui();
  _particles.setup();
  //Easy cam - nice iniitial camera view -> use debug->dumpCamPos in UI to get new positions
  _easyCam.setPosition(32.279f, 15.6371f, 19.3431f);
  _easyCam.setTarget(vec3(0.0f));
}

//--------------------------------------------------------------
void ofApp::update(){
  _particles.update(false);
  _ui.status.mode->setLabel(_particles.getMode());
  _ui.status.stats->setLabel(_particles.getStats());
}

//--------------------------------------------------------------
void ofApp::draw(){
  _easyCam.begin();
  if (_ui.view.showGrid->getChecked()) {
    ofDrawGrid();
  }
  _particles.draw();
  if (_ui.view.showWalkLimit->getChecked()) {
    _drawWalkLimit();
  }
  _easyCam.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch (key) {
    case ' ':
      //Show / Hide GUI
      _ui.display = !_ui.display;
      _ui.gui->setAutoDraw(_ui.display);
      break;
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

#pragma mark private methods

void ofApp::_setupGui() {
  
  //Create GUI and assign appropriate events etc
  _ui.gui = new ofxDatGui( ofxDatGuiAnchor::TOP_RIGHT);
  
  _ui.gui->setTheme(new ofxDatGuiThemeCharcoal);
  
  _ui.gui->addHeader("::Particle Lab::");
  _ui.gui->addLabel("PHASE: INIT");
  
  //Status Folder
  _ui.status.folder = _ui.gui->addFolder("STATUS", ofColor::hotPink);
  // -> main status
  _ui.status.mode = _ui.status.folder->addLabel("<STATUS>");
  // -> counters
  _ui.status.stats = _ui.status.folder->addLabel("C:n L:n P:n ∑P:n");
  _ui.status.folder->expand();
  
  // -> regenerate
  _ui.status.regenerate = _ui.status.folder->addButton("regenerate...");
  _ui.status.regenerate->onButtonEvent(this, &ofApp::_onButtonEvent);
  
  
  //View Folder
  _ui.view.folder = _ui.gui->addFolder("VIEW", ofColor::white);
  _ui.view.showGrid = _ui.view.folder->addToggle("Show Grid");
  _ui.view.showGrid->setChecked(false);       //Don't show grid by default
  _ui.view.showWalkLimit = _ui.view.folder->addToggle("show.walkLimit");
  _ui.view.showWalkLimit->setChecked(true);
  _ui.view.folder->expand();
  
  //Colors Folder
  _ui.view.locked = _ui.view.folder->addColorPicker("locked");
  _ui.view.locked->setColor(ofColor::white);
  _ui.view.unlocked = _ui.view.folder->addColorPicker("unlocked");
  _ui.view.unlocked->setColor(_yellow);

  //Generation Folder
  _ui.generation.folder = _ui.gui->addFolder("GENERATION", ofColor::greenYellow);
  // -> rootSize
  _ui.generation.rootSize = _ui.generation.folder->addSlider("root.size",0.01f,1.0f,0.8f);
  _ui.generation.rootSize->setPrecision(2);
  // -> tailSize
  _ui.generation.tailSize = _ui.generation.folder->addSlider("tail.size",0.01f,1.0f,0.1f);
  _ui.generation.tailSize->setPrecision(2);
  //TODO: Add control for picking curve from root to tail
  // -> number generations
  _ui.generation.generations = _ui.generation.folder->addSlider("n.generations",3,200,10);
  _ui.generation.generations->setPrecision(0);
  // -> number particles
  _ui.generation.particles = _ui.generation.folder->addSlider("n.particles",10,50,12);
  _ui.generation.particles->setPrecision(0);
  // -> walk bias
  _ui.generation.walkBias = _ui.generation.folder->addSlider("walk.bias",0.0f,1.0f,0.02f);
  _ui.generation.walkBias->setPrecision(2);
  // -> walk limit
  _ui.generation.walkLimit = _ui.generation.folder->addSlider("walk.limit",50,500,250);
  _ui.generation.walkLimit->setPrecision(0);
  
  _ui.generation.folder->expand();
  
  //Replication Folder
  _ui.replication.folder = _ui.gui->addFolder("REPLICATION", ofColor::aliceBlue);
  // -> layers
  _ui.replication.layers = _ui.replication.folder->addSlider("n.layers",1,100,1);
  _ui.replication.layers->setPrecision(0);
  
  _ui.replication.folder->expand();
  
  
  //Animation Folder
  
  
  //Debug Folder
  _ui.debug.folder = _ui.gui->addFolder("DEBUG", ofColor::goldenRod);
  _ui.debug.dumpCamPosition = _ui.debug.folder->addButton("dump cam.pos");
  _ui.debug.dumpCamPosition->onButtonEvent(this, &ofApp::_onButtonEvent);

  
  _ui.gui->addFRM();
  
}

void ofApp::_regenerateParticleSystem() {
  cout << "Regenerate!" << endl;
  _particles.reset(
    _ui.generation.rootSize->getValue(),
    _ui.generation.tailSize->getValue(),
    _ui.generation.generations->getValue(),
    _ui.generation.particles->getValue(),
    _ui.generation.walkBias->getValue(),
    _ui.generation.walkLimit->getValue(),
    _ui.replication.layers->getValue(),
    _ui.view.unlocked->getColor(),
    _ui.view.locked->getColor()
  );
}

void ofApp::_drawWalkLimit() {
  float n = 20;
  float t = TWO_PI / n;
  float r = _ui.generation.walkLimit->getValue();
  ofPushStyle();
  ofSetColor(255,0,0);
  for (float i=0; i<n; i++) {
    ofDrawSphere(cos(i * t) * r, 0.0f, sin(i* t) * r,  0.1f);
  }
  //Draw an upper and lower marker to help us navigate a bit better
  ofSetColor(0,255,0);  //
  ofDrawArrow(vec3(0.0f,-r,0.0f),vec3(0.0f,r,0.0f),0.1f);
  ofPopStyle();
}

#pragma mark gui handler events

void ofApp::_onToggleEvent(ofxDatGuiToggleEvent e) {
  //handle toggles from various controls
}

void ofApp::_onButtonEvent(ofxDatGuiButtonEvent e) {
  //handle toggles from various controls
  if (e.target == _ui.status.regenerate) {
    _regenerateParticleSystem();
  }
  if (e.target == _ui.debug.dumpCamPosition) {
    cout << "cam.pos " << _easyCam.getPosition() << endl;
  }
}



