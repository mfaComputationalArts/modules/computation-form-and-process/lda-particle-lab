//
//  PartricleSystem.hpp
//  01_ParticleLab_lxinspc
//
//  Created by Nathan Adams on 03/02/2020.
//

#ifndef ParticleSystem_hpp
#define ParticleSystem_hpp

#include "ofMain.h"
#include "Particle.hpp"
#include "MathHelpers.hpp"

using namespace glm;

class ParticleSystem {
public:

  enum Mode {
    INIT,
    GENERATION,
    REPLICATION,
    ANIMATION,
    PAUSED
  };

  void setup();
  void reset(float rootSize, float tailSize, int nGenerations, int nParticles, float walkBias, float walkLimit, int nLayers, ofFloatColor uColor, ofFloatColor lColor);
  void update(bool isStep);
  void draw();
  string getMode();
  string getStats();
private:
  //Methods
  void _updateGeneration();
  void _placeParticles();
  void _updateReplication();
  void _updateAnimation();
  //Propertites
  vector<Particle> _particles;                  //Vector of particles created
  //Flags and counters
  Mode _mode = GENERATION;
  Mode _steppedMode;
  
  struct GenerationWalk {
    float b;              //Walk Bias
    float l;              //Walk limit
  };
  
  struct Size {
    float r;              //Root size
    float t;              //Tail size
  };
  
  //Generation parameters
  struct Generation {
    int n;                //Number generations
    int c;                //Current Generation
    int p;                //Number particles per generation
    int u;                //Current number unlocked particles
    float m2;             //Max squared distance of all locked particles
    GenerationWalk w;
    Size s;
  };
  
  struct Replication {
    int l;                //Number layers
    int c;                //Current layer
  };
  
  struct Seed {
    Generation g;
    Replication r;
  };
  
  Seed _seed;
  
  struct Stats {
    int g;                //Generation counter
    int p;                //Number particles this layer
    int c;                //Current layer
    int l;                //Number layers create
    int sP;               //Total number particles
  };
  
  Stats _stats;
  
  float _getSizeByGeneration(int g);
  vec3 _getRandom2DPosition(float k, float l);
  vec3 _getRandom3DPosition(float k, float l);
  
  vec3 _origin = vec3(0.0f,0.0f,0.0f);
  
  ofFloatColor _lockColor;
  ofFloatColor _unlockColor;
  
};


#endif /* ParticleSystem_hpp */
