//
//  PartricleSystem.cpp
//  01_ParticleLab_lxinspc
//
//  Created by Nathan Adams on 03/02/2020.
//

#include "ParticleSystem.hpp"

// Non paramtesized values
#define ROOT_SIZE 0.5f
#define TAIL_SIZE 0.01f
#define RANDOM_PLACE_MIN 5.0f
#define RANDOM_PLACE_MAX 15.0f

#pragma mark public / life cycle methods

void ParticleSystem::setup() {
  //Nothing really to do, we set everything up in header definitions, and start GENERATION when defineSeed is called
  _mode = INIT;
}

void ParticleSystem::reset(float rootSize, float tailSize, int nGenerations, int nParticles, float walkBias, float walkLimit, int nLayers, ofFloatColor uColor, ofFloatColor lColor) {
  //Resets the particle system - deleting all particles, and any associated flags and counters
  _particles.clear();
  _mode = GENERATION;           //Return to generation phase
  //Generation Parameters
  _seed.g.n = nGenerations;     //Number of generations
  _seed.g.c = 0;                //Current generation
  _seed.g.p = nParticles;       //Number particles to create per generation
  _seed.g.u = 0;                //Number of particles unlocked in current generation
  _seed.g.w.b = walkBias;       //Strength of bias to origin
  _seed.g.w.l = walkLimit;      //How to stray from origin before deletion
  _seed.g.s.r = rootSize;
  _seed.g.s.t = tailSize;
  _seed.g.m2 = 0;
  //Set Colors
  _unlockColor = uColor;
  _lockColor = lColor;
  //Create a central seed particle at 0,0,0 with size rootSize
  Particle root;
  root.setup({
    .generation = 0,
    .particle = 0
  },_origin,_getSizeByGeneration(0),true, _unlockColor, _lockColor );
  _particles.push_back(root);
}

void ParticleSystem::update(bool isStep) {
  if (isStep) {
    //Reset out of PAUSED mode for one update cycle
    _mode = _steppedMode;
  }
  switch (_mode) {
    case GENERATION:
      _updateGeneration();
      break;
    case REPLICATION:
      _updateReplication();
      break;
    case ANIMATION:
      _updateAnimation();
      break;
    case INIT:
    case PAUSED:
      //Do nothing - allows us to step through the animation
      break;
  }
  if (isStep) {
    //As a single step, we keep the mode we had - and set to paused (we can then reinstate on next step)
    _steppedMode = _mode;
    _mode = PAUSED;
  }
}

void ParticleSystem::draw() {
  for (int i=0; i<_particles.size();i++) {
    _particles[i].draw();
  }
}

string ParticleSystem::getMode() {
  switch (_mode) {
    case INIT:
      return "INIT";
      break;
    case GENERATION:
      return "GENERATION";
      break;
    case REPLICATION:
      return "REPLICATION";
      break;
    case ANIMATION:
      return "ANIMATION";
      break;
    case PAUSED:
      return "PAUSED";
      break;
  }
}

string ParticleSystem::getStats() {
  string _r;
  switch (_mode) {
    case INIT:
      _r = "--/--/--/--";
      break;
    case GENERATION:
      _r = ofToString(_stats.g,0,2,'0');
      break;
    case REPLICATION:
      break;
    case ANIMATION:
      break;
    case PAUSED:
      break;
  }
  return _r;
}

#pragma mark private methods


void ParticleSystem::_updateGeneration() {
  //During generation particles are created and placed at random locations, and random walked until they hit a locked particle
  //During this phase we first to check to see if all particles from the last generation have been placed yet, if they have
  //we then need to place more particles, and decrease the generation counter
  if (ofGetFrameNum() % 1 == 0) {
    _stats.g++;
    
    if ( _seed.g.u <= 0 ) {
      if (_seed.g.c > _seed.g.n) {
        _mode = REPLICATION;
      } else {
        _placeParticles();
      }
    }
    
    for (int i=0; i<_particles.size();i++) {
      if (!_particles[i].getLocked()) {
        _particles[i].updateRandomWalk(_seed.g.w.b, _seed.g.w.l);    //Random walk the particle
        //have we gone past our limit?
        if (!_particles[i].getOutOfBounds()) {
          //Dont check out of bounds particles
          //check for locked particle collisions - version 1.0 - don't care about unlocked particle collisions
          //TODO: have unlocked particles collide and form a joined particle which can continue it's random walk!
          for (int j=0;j<_particles.size();j++) {
            if (_particles[j].getLocked()) {
              //only locked particles to check (also check i hasn't been locked already - no need to calculate distance if so
              if (!_particles[i].getLocked() && distSquared(_particles[i].getPos(),_particles[j].getPos()) <= square(_particles[i].getSize() + _particles[j].getSize())) {
                //this is now a locked particle
                _particles[i].setLocked(true);
                float _m = distSquared(_particles[i].getPos(),_origin);
                if (_m > (_seed.g.m2 * _seed.g.m2)) {
                  _seed.g.m2 = sqrt(_m);
                  cout << "updated m2 for g " << _seed.g.c << " to " << _seed.g.m2 << endl;
                }
                _seed.g.u--;
              }
            }
          }
        }
      }
    }
  }
  
  
}

void ParticleSystem::_placeParticles() {
  //Version 1 - particle placement is purely random -
  //TODO: add additional possibilites for placement
  //Place _generationParticles count of new particles randomly on the grid at X 0.5->1, y 0.5->1, z 0
  _seed.g.u = _seed.g.p;
  cout << "GENERATION: Placing " << _seed.g.p << " particles, for generation " << _seed.g.c << endl;
  for (int i = 0; i < _seed.g.p; i++) {
    Particle p;
    p.setup({
        .generation = _seed.g.c,
        .particle = i
      },
      _getRandom2DPosition(RANDOM_PLACE_MIN + _seed.g.m2, RANDOM_PLACE_MAX + _seed.g.m2),
      _getSizeByGeneration(_seed.g.c),
      false,
      _unlockColor,
      _lockColor);
    _particles.push_back(p);
  }
  //Decrease the generation counter, and check to see if we have run all generations
  _seed.g.c++;
}

void ParticleSystem::_updateReplication() {
  
}

void ParticleSystem::_updateAnimation() {
  
}


float ParticleSystem::_getSizeByGeneration(int g) {
  //calculates size based on which generation and interpolation between root and tail sizes
  return deNormalise(normalise(g,0,_seed.g.n),_seed.g.s.r, _seed.g.s.t);
}

vec3 ParticleSystem::_getRandom2DPosition(float k, float l) {
  float r = ofRandom(k,l);
  float a = ofRandom(TWO_PI);
  return vec3(cos(a) * r,0.0f,sin(a) * r);
}

vec3 ParticleSystem::_getRandom3DPosition(float k, float l) {
  return vec3(ofRandom(k,l),ofRandom(k,l),ofRandom(k,l));
}
