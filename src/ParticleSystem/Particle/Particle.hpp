//
//  Particle.hpp
//  01_ParticleLab_lxinspc
//
//  Created by Nathan Adams on 03/02/2020.
//

#ifndef Particle_hpp
#define Particle_hpp

#include "ofMain.h"
#include "MathHelpers.hpp"

using namespace glm;

class Particle {
public:
  //Id allows us to keep track of which particle is which
  struct Id {
    int generation;
    int particle;
  };
  void setup(Id id, vec3 initPos,float size, bool locked, ofFloatColor uColor, ofFloatColor lColor);
  void updateRandomWalk(float bias, float oobDistance);
  void updateAnimation();
  void draw();
  
  void setLocked(bool l);
  bool getLocked();
  void setOutOfBounds(bool oob);
  bool getOutOfBounds();
  float getSize();
  vec3 getPos();
  
  float getDistanceFromOrigin();
  
  
private:
  Id _id;
  float _size;
  vec3 _pos;
  bool _locked;
  bool _oob;
  
  vec3 _origin = vec3(0.0f, 0.0f, 0.0f);
  
  ofFloatColor _unlockColor;
  ofFloatColor _lockColor;
  
  ofSpherePrimitive _sphere;
};

#endif /* Particle_hpp */
