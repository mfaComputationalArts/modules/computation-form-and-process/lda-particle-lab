//
//  Particle.cpp
//  01_ParticleLab_lxinspc
//
//  Created by Nathan Adams on 03/02/2020.
//

#include "Particle.hpp"

#pragma mark public / lifecycle methods

#define WALK_RATE 0.1f

void Particle::setup(Id id, vec3 initPos, float size, bool locked, ofFloatColor uColor, ofFloatColor lColor) {
  _id = id;
  _size = size;
  _locked = locked;
  _lockColor = lColor;
  _unlockColor = uColor;
  _pos = initPos;
}

void Particle::updateRandomWalk(float bias, float oobDistance) {
  if (!_locked) {
    //Don't update locked particles :)
    //create a unit vector in a random direction, then add to the unit vector to the origin  ultiplied by the bias
    //TODO: for version 1 - these are all 2D vectors applied to a consistent Z, we will update these later to be 3D vectors
    float t = ofRandom(TWO_PI);
    vec3 w = vec3(cos(t),0,sin(t)) * WALK_RATE;       //Random point on the xz plane
    vec3 o = (_origin - _pos) * bias;                 //Origin, is 0 - _pos
    vec3 d = o + w;
    //update position
    _pos = _pos + d;
    //cout << "D " << d << endl;
    //Out of bounds?
    _oob = distSquared(_origin, _pos) > square(oobDistance);
  }
}

void Particle::updateAnimation() {
  
}

void Particle::draw() {
  ofPushStyle();
  ofFloatColor _color;
  if (_locked) {
    _color = _lockColor.getLerped(ofColor::darkGray, ofMap(_id.generation,0,200,0.2,1.0,true));
  } else {
    _color = _unlockColor;
  }
  ofSetColor(_color);
  ofDrawSphere(_pos,_size);
  ofPopStyle();
}

#pragma mark getters / setters
void Particle::setLocked(bool l) {
  _locked = l;
  cout << "Particle ID " << _id.generation << "->" << _id.particle << " locked at pos " << _pos << endl;
}

bool Particle::getLocked() {
  return _locked;
}

void Particle::setOutOfBounds(bool oob) {
  _oob = oob;
}

bool Particle::getOutOfBounds() {
  return _oob;
}

float Particle::getSize() {
  return _size;
}

vec3 Particle::getPos() {
  return _pos;
}


#pragma mark private methods



