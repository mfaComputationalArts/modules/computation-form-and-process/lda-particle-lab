//
//  MathHelpers.hpp
//  01_ParticleLab_lxinspc
//
//  Created by Nathan Adams on 03/02/2020.
//

#ifndef MathHelpers_hpp
#define MathHelpers_hpp

#include "ofMain.h"

using namespace glm;

float distSquared(vec3 a, vec3 b);
float square(float x);

float normalise(float i, float l, float u);

float deNormalise(float n, float u);
float deNormalise(float n, float l, float u);

#endif /* MathHelpers_hpp */
