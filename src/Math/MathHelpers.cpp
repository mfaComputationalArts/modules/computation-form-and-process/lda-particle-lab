//
//  MathHelpers.cpp
//  01_ParticleLab_lxinspc
//
//  Created by Nathan Adams on 03/02/2020.
//

#include "MathHelpers.hpp"

float distSquared(vec3 a, vec3 b) {
  vec3 t = a - b;
  return dot(t, t);
}

float square(float x) {
  return x * x;
}


float normalise(float i, float l, float u) {
  return (i - l) / (u - l);
}


float deNormalise(float n, float u) {
  return deNormalise(n, 0.0f, u);
}

float deNormalise(float n, float l, float u) {
  return ofMap(n,0.0f,1.0f,l,u,true);
}
