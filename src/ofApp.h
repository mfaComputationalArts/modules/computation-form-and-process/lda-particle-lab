#pragma once

#include "ofMain.h"
#include "ofxDatGui.h"
#include "ParticleSystem.hpp"

class ofApp : public ofBaseApp{
  
public:
  void setup();
  void update();
  void draw();
  
  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseEntered(int x, int y);
  void mouseExited(int x, int y);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);
  
  
private:
  
  //Easy Cam
  ofEasyCam _easyCam;
  
  //Particle System
  ParticleSystem _particles;
  
  void _regenerateParticleSystem();
  
  ofFloatColor _yellow = ofFloatColor(0.8431f, 0.9922f, 0.3882f, 1.0f);
  
  //GUI - note using ofxDatGui rather than ofxGui - requires the folder ofxbraitsch to be copied from addOns/ofxDatGui to bin/data
  void _setupGui();

  struct _UI_Status {
    ofxDatGuiFolder* folder;
    ofxDatGuiLabel* mode;
    ofxDatGuiLabel* stats;
    ofxDatGuiButton* regenerate;
  };
  
  struct _UI_View {
    ofxDatGuiFolder* folder;
    ofxDatGuiToggle* showGrid;
    ofxDatGuiToggle* showWalkLimit;
    ofxDatGuiColorPicker* unlocked;
    ofxDatGuiColorPicker* locked;
  };
  
  struct _UI_Generation {
    ofxDatGuiFolder* folder;
    ofxDatGuiSlider* rootSize;
    ofxDatGuiSlider* tailSize;
    //TODO: Add control for picking curve from root to tail
    ofxDatGuiSlider* generations;
    ofxDatGuiSlider* particles;
    ofxDatGuiSlider* walkBias;
    ofxDatGuiSlider* walkLimit;
    //TODO: Add control for walk shape (assume circular in v1.0)
    //TODO: Add control for 2D/3D walks
  };

  struct _UI_Replication {
    ofxDatGuiFolder* folder;
    ofxDatGuiSlider* layers;
  };

  
  struct _UI_Debug {
    ofxDatGuiFolder* folder;
    ofxDatGuiButton* dumpCamPosition;
  };
  
  struct _UI {
    bool display = true;
    ofxDatGui* gui;
    _UI_Status status;
    _UI_View view;
    _UI_Generation generation;
    _UI_Replication replication;
    _UI_Debug debug;
  };
  
  
  _UI _ui;
  
  void _onToggleEvent(ofxDatGuiToggleEvent e);
  void _onButtonEvent(ofxDatGuiButtonEvent e);
  
  //View display flags
  
  void _drawWalkLimit();
  
};
